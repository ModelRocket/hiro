# build stage
FROM golang:alpine AS build-env

RUN  mkdir -p /go/src \
    && mkdir -p /go/bin \
    && mkdir -p /go/pkg

ENV GOPATH=/go
ENV PATH=$GOPATH/bin:$PATH
ENV PKGROOT=$GOPATH/src/gitlab.com/ModelRocket/hiro

# now copy your app to the proper build path
RUN mkdir -p $PKGROOT
ADD . $PKGROOT

# should be able to build now
WORKDIR $PKGROOT
RUN go build -o $GOPATH/bin/hiro .

# final stage
FROM golang:alpine
RUN mkdir /work
WORKDIR /work
ENV GOPATH=/go
ENV PATH=$GOPATH/bin:$PATH
COPY --from=build-env $GOPATH/bin/hiro /app/
ENV GOPATH=/go
ENTRYPOINT ["/app/hiro"]