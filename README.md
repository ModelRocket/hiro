# hiro
Hiro (snowcrash) is the Model Rocket project generation tool. Hiro borrows heavily
from [go-swagger](https://goswagger.io), but builds other custom components for projects
using the [Model Rocket Cloud Core](https://gitlab.com/ModelRocket/sparks).
  
## Install

Hiro uses go modules ensure the env var `GO111MODULE=on` is set in your `.bash_profile` or other appropriate location.

```
> go install gitlab.com/ModelRocket/hiro/cmd/hiro
```

## Project Layout
All hiro projects have the same base layout:

```
~~~
├── .hiro                     // The hiro project dir
├── api/                      // Generated API sources
│   └── api_name/             // The api name
│       └── rest/                 // The swagger based rest api
|       └── types/                // The api data types and models
|   └── api_name.yaml          // The swagger definition
├── functions/                                      
│   └── .../                  // Cloud functions
├── LICENSE                   // The project license

~~~
```

## Getting started
You run hiro from with the directory you want to create a new project. This directory
must be within your $(GOPATH)/src. To initialize a project you simply point it at
your swagger document. This can be a file or a url, either will be copied to
`api/swagger.yaml`

*Note*: hiro cannot resolve private swagger documents at this time, so all definitions must
be public or contained within the same specification document. For projects that use domains
or external references, setup a GitHub sync in swagger to the git branch or the project
to the location above.

### Initialize the project
```
> hiro init
```

### Working with an API
#### Add an API
```
 > hiro api add https://api.swaggerhub.com/apis/ModelRocket/acme/1.0.0/swagger.yaml
```

#### Sync the API
If you make changes to the API, you need to sync the swagger.
```
> hiro api sync
```

#### Create a cloud entry point for a specific API (lambda or otherwise)
```
> hiro function add acme
```

#### Run a function

`hiro` will run any method and simulate an aws lambda proxy, or simply execute it with the specified environment. 
Most commonly you want to build and run the function with API Gateway proxy support.

```
> hiro run --proxy=apigw --build ./functions/acme
```

#### Proxy Types

You use the `proxy` parameter to tell `hiro` how to handle the incoming data.

| Proxy    | Description                                                                                                                                                                    |
| -------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `apigw`* | This will marshal incoming HTTP request into a `events.APIGatewayProxyRequest` object and return the expected result after unmarshaling from `events.APIGatewayProxyResponse`. |
| `raw`    | This will pass the raw json event to the lambda handler as []byte.                                                                                                             |
| `none`   | This will not start a proxy, only execute the function and wait for it to complete.                                                                                            |
\* `apigw` is the default proxy type.

#### Environments
You can provide environment variables to a hiro function via the `hiro.json` file.

```json
{
	"environment": {
		"prod": {
			"CLOUD_PROVIDER": "aws",
			"DB_DRIVER": "postgres"
		}
	}
}
```

You can use this environment (the default is dev) by specifying in with `run`

```
> hiro run -e prod --proxy=apigw --build ./functions/acme
```

### Docker

You can use the hiro docker in two ways.


#### Docker CLI

You can use it as a cli, but you need to share your local GOTPATH volume as well as set the working dir relative to the container volume. Keep in mind this is a very large image.

```
> docker run -v $GOPATH:/go -w /go/src/gitlab.com/decanter/decanter-api registry.gitlab.com/modelrocket/hiro/hiro-cli run --build ./functions/api
```

#### Hiro Container

You should use `dep` to vendor your project first to ensure it can be built in a container.

The you can use the `examples` as starting points for your apps.

##### Passing config to the container

When running your container you can pass the hiro config like this:

```
> docker run -v `pwd`:/config -e LOG_FORMAT=text -e LOG_LEVEL=debug -p 9000:9000 registry.gitlab.com/acme/acme-api:latest
```

This makes the current directory the `/config` in the container and hiro will look for `hiro.json` there.