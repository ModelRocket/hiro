/*************************************************************************
 * MIT License
 * Copyright (c) 2018 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"

	"github.com/go-openapi/loads"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/validate"
	"github.com/gosimple/slug"
	"github.com/manifoldco/promptui"
	"github.com/teris-io/shortid"
	"github.com/urfave/cli"
)

func specCheckErrors(spec *loads.Document) (warn error, err error) {
	validate.SetContinueOnErrors(false)

	v := validate.NewSpecValidator(spec.Schema(), strfmt.Default)
	result, _ := v.Validate(spec)

	if result.HasWarnings() {
		str := fmt.Sprint("The spec validation had the following warnings: \n")
		for _, desc := range result.Warnings {
			str += fmt.Sprintf("- WARNING: %s\n", desc.Error())
		}
		warn = errors.New(str)
	}
	if result.HasErrors() {
		str := fmt.Sprint("The spec validation had the following errors: \n")
		for _, desc := range result.Errors {
			str += fmt.Sprintf("- %s\n", desc.Error())
		}
		err = errors.New(str)
	}

	return
}

func specValidate(filename string) (*loads.Document, error) {
	log.Infof("Validating spec at %q", filename)

	spec, err := loads.Spec(filename)
	if err != nil {
		return nil, err
	}

	if warn, err := specCheckErrors(spec); err != nil {
		return nil, err
	} else if warn != nil {
		log.Warnln(warn)
	} else {
		log.Infof("The spec validated with no warnings or errors.")
	}
	return spec, nil
}

func actionSpecValidate(c *cli.Context) error {
	if c.NArg() < 1 {
		return errors.New("missing api name argument")
	}

	_, err := specValidate(project.SpecPath(c.Args().First()))

	return err
}

func actionSpecAdd(c *cli.Context) error {
	var specURL string
	apiSpec := APISpec{}

	if c.NArg() > 0 {
		specURL = c.Args().Get(0)
	} else {
		return errors.New("missing spec url")
	}

	apiName := ""

	// if we have a url then fetch the file
	if _, err := url.ParseRequestURI(specURL); err == nil {
		spec, err := actionSpecFetch(specURL, c)
		if err != nil {
			return err
		}
		apiSpec.Source = specURL
		apiSpec.Spec = spec.SpecFilePath()
		apiName = specName(spec)
	} else {
		log.Info("Validating the spec...")

		spec, err := loads.Spec(specURL)
		if err != nil {
			return err
		}

		if warn, err := specCheckErrors(spec); err != nil {
			return err
		} else if warn != nil {
			log.Warnln(warn)
		}

		apiSpec.Spec = specURL
		apiName = specName(spec)
	}

	if project.APIs == nil {
		project.APIs = make(map[string]*APISpec)
	}
	project.APIs[apiName] = &apiSpec
	project.saveOnExit = true

	return nil
}

func actionSpecSync(c *cli.Context) error {
	apis := make([]string, 0)
	if c.NArg() > 0 {
		apis = append(apis, c.Args().Get(0))
	} else {
		for k, v := range project.APIs {
			if v.AutoSync == nil || *v.AutoSync == true {
				apis = append(apis, k)
			}
		}
	}

	for _, specName := range apis {
		specDef, ok := project.APIs[specName]
		if !ok {
			return fmt.Errorf("api %s not defined", specName)
		}

		if specDef.Source == "" {
			return errors.New("spec source undefined")
		}

		if _, err := actionSpecFetch(specDef.Source, c); err != nil {
			return err
		}
		if c.BoolT("generate") {
			apiGenerate(specName, "", false)
		}
	}

	return nil
}

func actionSpecFetch(specURL string, c *cli.Context) (*loads.Document, error) {
	// attempt to download and validate the spec
	client := http.Client{}
	req, _ := http.NewRequest(c.String("http-method"), specURL, nil)
	if len(c.String("auth-token")) > 0 {
		req.Header.Add(c.String("auth-header"), c.String("auth-token"))
	}

	log.Infof("Fetching the spec from %s...", specURL)
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("failed to get swagger: %s", string(bytes))
	}

	// create a temporary working directory
	tmpDir, err := ioutil.TempDir("", shortid.MustGenerate())
	if err != nil {
		return nil, err
	}
	defer os.RemoveAll(tmpDir)

	tmpPath := filepath.Join(tmpDir, "swagger.yaml")
	if err := ioutil.WriteFile(tmpPath, bytes, 0666); err != nil {
		return nil, err
	}

	log.Info("Validating the spec...")

	spec, err := loads.Spec(tmpPath)
	if err != nil {
		return nil, err
	}

	if warn, err := specCheckErrors(spec); err != nil {
		return nil, err
	} else if warn != nil {
		log.Warnln(warn)
	}

	overwrite := true

	outfile := fmt.Sprintf("%s/%s.yaml", c.String("output"), specName(spec))

	if tmp := project.SpecPath(specName(spec)); tmp != "" {
		outfile = tmp
	}

	if _, err := os.Stat(outfile); err == nil && !c.Bool("overwrite") {
		log.Warnf("There is an existing specfile at %q", outfile)

		prompt := promptui.Prompt{
			Label:   "Overwrite? (y/n)",
			Default: "N",
		}

		result, err := prompt.Run()
		if err != nil {
			return nil, err
		}
		if strings.ToUpper(result) != "Y" {
			overwrite = false
		}
	}

	if overwrite {
		log.Infof("Saving spec file to %q", outfile)
		// Save the project file now that is validated
		if err := writeFile(outfile, bytes, 0644); err != nil {
			return nil, err
		}
	}
	spec, err = loads.Spec(outfile)
	if err != nil {
		return nil, err
	}

	return spec, nil
}

func specName(spec *loads.Document) string {
	return packageName(slug.Make(spec.Spec().Info.Title))
}
