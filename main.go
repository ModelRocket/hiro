/*************************************************************************
 * MIT License
 * Copyright (c) 2018 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package main

import (
	"os"

	"github.com/urfave/cli"

	"gitlab.com/ModelRocket/reno/logger"
)

var (
	project = &Project{}
	log     = logger.New("hiro")
)

func main() {
	app := cli.NewApp()

	app.Name = "hiro"
	app.Usage = "Model Rocket Cloud Service Tool"
	app.Version = "1.6.0"

	app.Flags = []cli.Flag{
		cli.BoolFlag{
			Name:  "debug, d",
			Usage: "Debug hiro",
		},
		cli.StringFlag{
			Name:   "config-dir",
			Usage:  "hiro config dir",
			Value:  DefaultProjectDirectory,
			EnvVar: "HIRO_CONFIG_DIR",
		},
		cli.StringFlag{
			Name:   "env,e",
			Usage:  "The environment to us",
			Value:  "dev",
			EnvVar: "HIRO_ENV",
		},
	}

	app.Before = actionLoadProject
	app.After = actionSaveProject

	app.Commands = []cli.Command{
		cli.Command{
			Name:   "init",
			Usage:  "Initialize the project and licensess",
			Action: actionProjectInit,
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "license-type",
					Usage: "Set the project license type",
					Value: "Unlicensed",
				},
				cli.StringFlag{
					Name:  "license-owner",
					Usage: "Set the project license owner",
					Value: "Model Rocket LLC",
				},
			},
		},
		cli.Command{
			Name:  "env",
			Usage: "Environment variable stuff",
			Subcommands: []cli.Command{
				cli.Command{
					Name:   "export",
					Usage:  "Export the environment from the specified name environment",
					Action: actionEnvExport,
				},
				cli.Command{
					Name:      "set",
					Usage:     "Set an env var",
					ArgsUsage: "<name> <value>",
					Flags: []cli.Flag{
						cli.BoolFlag{
							Name:  "encrypt,E",
							Usage: "Encrypt the value",
						},
						cli.StringFlag{
							Name:   "key",
							Usage:  "The encryption key to use",
							EnvVar: "HIRO_ENC_KEY",
						},
					},
					Action: actionEnvSet,
				},
				cli.Command{
					Name:      "get",
					Usage:     "Get an env var",
					ArgsUsage: "<name>",
					Flags: []cli.Flag{
						cli.BoolFlag{
							Name:  "decrypt,D",
							Usage: "Decrypt the value",
						},
					},
					Action: actionEnvGet,
				},
			},
		},
		cli.Command{
			Name:  "api",
			Usage: "Manage APIs",
			Subcommands: []cli.Command{
				cli.Command{
					Name:      "add",
					Usage:     "Add swagger api to the project",
					ArgsUsage: "<url>",
					Flags: []cli.Flag{
						cli.StringFlag{
							Name:  "http-method",
							Usage: "The HTTP method to use when fetching the remote spec",
							Value: "GET",
						},
						cli.StringFlag{
							Name:  "auth-header",
							Usage: "The HTTP authorization header to use when fetching the remote spec",
							Value: "Authorization",
						},
						cli.StringFlag{
							Name:   "auth-token",
							Usage:  "The HTTP authorization token to use when fetching the remote spec",
							EnvVar: "SWAGGER_API_KEY",
						},
						cli.StringFlag{
							Name:  "output,o",
							Usage: "The output path",
							Value: "./api",
						},
						cli.BoolFlag{
							Name:  "local",
							Usage: "The path speficied is a local file",
						},
						cli.BoolFlag{
							Name:  "force",
							Usage: "Overwrite the existing api spec with the one downloaded",
						},
					},
					Action: actionSpecAdd,
				},
				cli.Command{
					Name:      "sync",
					Usage:     "sync the api from source",
					ArgsUsage: "<api name>",
					Action:    actionSpecSync,
					Flags: []cli.Flag{
						cli.StringFlag{
							Name:  "http-method",
							Usage: "The HTTP method to use when fetching the remote spec",
							Value: "GET",
						},
						cli.StringFlag{
							Name:  "auth-header",
							Usage: "The HTTP authorization header to use when fetching the remote spec",
							Value: "Authorization",
						},
						cli.BoolTFlag{
							Name:  "generate",
							Usage: "Perform api generation after sync",
						},
						cli.StringFlag{
							Name:  "output,o",
							Usage: "The output path",
							Value: "./api",
						},
						cli.StringFlag{
							Name:   "auth-token",
							Usage:  "The HTTP authorization token to use when fetching the remote spec",
							EnvVar: "SWAGGER_API_KEY",
						},
						cli.BoolTFlag{
							Name:  "overwrite,y",
							Usage: "overwrite the swagger if it is valid",
						},
					},
				},
				cli.Command{
					Name:      "validate",
					Usage:     "Validate the api spec",
					ArgsUsage: "<api name>",
					Action:    actionSpecValidate,
				},
				cli.Command{
					Name:      "generate",
					Usage:     "Generates the api opertions, types, and implementation stubs",
					ArgsUsage: "<api name>",
					Flags: []cli.Flag{
						cli.BoolFlag{
							Name:  "force",
							Usage: "Force overwrite all source",
						},
						cli.StringFlag{
							Name:  "output,o",
							Usage: "The output path",
						},
					},
					Action: actionAPIGenerate,
				},
			},
		},
		cli.Command{
			Name:    "function",
			Aliases: []string{"fn"},
			Usage:   "Manage API functions",
			Subcommands: []cli.Command{
				cli.Command{
					Name:      "add",
					Aliases:   []string{"modify"},
					Usage:     "add or updates a cloud function entry stub",
					ArgsUsage: "<name>",
					Flags: []cli.Flag{
						cli.StringFlag{
							Name:  "api,a",
							Usage: "The api to add the function to",
						},
						cli.StringFlag{
							Name:  "output,o",
							Usage: "The output directory",
						},
						cli.BoolFlag{
							Name:  "force",
							Usage: "Force overwrite all source",
						},
					},
					Action: actionAddFunction,
				},
			},
		},
		cli.Command{
			Name:      "run",
			Usage:     "execute the command or function with a simulated the proxy environment",
			ArgsUsage: "<path>",
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:   "proxy,p",
					Usage:  "Specify the proxy type to simulate (apigw, raw, none)",
					Value:  "apigw",
					EnvVar: "HIRO_PROXY_MODE",
				},
				cli.StringFlag{
					Name:  "raw-return-type",
					Usage: "Specifiy the raw return type",
					Value: "application/json",
				},
				cli.StringFlag{
					Name:   "proxy-listen, l",
					Usage:  "Specify the proxy listen adress",
					Value:  "127.0.0.1:9000",
					EnvVar: "HIRO_LISTEN",
				},
				cli.BoolFlag{
					Name:   "grpc",
					Usage:  "Use grpc instead of go rpc",
					EnvVar: "HIRO_USE_GRPC",
				},
				cli.StringFlag{
					Name:   "rpc-port",
					Usage:  "Specifiy the port to use for RPC to the function",
					Value:  "10023",
					EnvVar: "_LAMBDA_SERVER_PORT",
				},
				cli.BoolTFlag{
					Name:  "build",
					Usage: "Build and execute the go source at the path",
				},
			},
			Action: actionExecFunction,
		},
		cli.Command{
			Name:   "export-templates",
			Hidden: true,
			Action: actionExportTemplates,
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "out",
					Value: "templates",
				},
			},
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatalln(err)
	}
}
