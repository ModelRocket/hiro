# Makefile use to build the dockers
SHIM_TAG ?= modelrocket/hiro/shim
CLI_TAG ?= modelrocket/hiro/hiro

all: dep docker-shim docker-cli

dep:
	dep ensure --update

docker-shim:
	@docker build -t registry.gitlab.com/$(SHIM_TAG) -f Dockerfile.shim .
	@docker push registry.gitlab.com/$(SHIM_TAG)

docker-cli: 
	@docker build -t registry.gitlab.com/$(CLI_TAG) .
	@docker push registry.gitlab.com/$(CLI_TAG)

.PHONY: \
	all