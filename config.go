/*************************************************************************
 * MIT License
 * Copyright (c) 2018 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package main

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"strings"
	"text/template"
	"time"

	abbr "github.com/BluntSporks/abbreviation"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli"
	"gitlab.com/ModelRocket/hiro/pkg/generator"
	"gitlab.com/ModelRocket/sparks"
	"gitlab.com/ModelRocket/sparks/providers/aws"
)

type (
	// License defines the project license type
	License struct {
		Owner     string `json:"owner,omitempty"`
		Type      string `json:"type,omitempty"`
		Header    string `json:"header,omitempty"`
		Copyright string `json:"-"`
	}

	// APISpec defines an API Specification
	APISpec struct {
		Spec      string `json:"spec,omitempty"`
		Source    string `json:"source,omitempty"`
		Principal string `json:"principal,omitempty"`
		// Version is the semantic version for the function
		Version string `json:"version,omitempty"`
		// Models is the models path
		Models        string `json:"models"`
		GenerateStubs bool   `json:"operation_stubs"`
		BasePath      string `json:"base"`
		WithContext   bool   `json:"with_context"`
		AutoSync      *bool  `json:"autosync"`
	}

	// Function defines a function settings
	Function struct {
		Source string `json:"source,omitempty"`
		Proxy  string `json:"proxy,omitempty"`
		Env    string `json:"env,omitempty"`
	}

	// Project is the hiro project file definition
	Project struct {
		// Name is the project name
		Name string `json:"name"`

		// APIs is the project apis
		APIs map[string]*APISpec `json:"apis"`

		// Keys is the encryption keys
		Keys map[string]string `json:"keys"`

		// Functions is the project functions
		Functions map[string]*Function `json:"functions"`

		// Generate using non-pointer arrays i.e. []type vs []*type
		ScalarArrays bool `json:"scalar_arrays"`

		// License is the license definition for the project
		License License `json:"license"`

		// Environment defines the global environment
		Environment map[string]map[string]string `json:"environment,omitempty"`

		valid      bool
		saveOnExit bool
	}
)

const (
	// DefaultProjectDirectory is the hiro project directory
	DefaultProjectDirectory = "."

	// ProjectFile is the default project file
	ProjectFile = "hiro.json"

	// DefaultSpecFile is the default api spec file
	DefaultSpecFile = "swagger.yaml"
)

func init() {
	abbr.Abbrevs["service"] = "svc"
}

func actionLoadProject(c *cli.Context) error {
	if c.GlobalBool("debug") {
		log.Level = logrus.DebugLevel
	}

	projectDir := c.String("config-dir")
	projectFile := path.Join(projectDir, ProjectFile)

	fd, err := os.Open(projectFile)
	if err != nil {
		if !os.IsNotExist(err) {
			log.Fatalln(err)
		}
		log.Errorf("Project file %q does not exist.", projectFile)
		return nil
	}
	defer fd.Close()

	data, err := ioutil.ReadAll(fd)
	if err != nil {
		log.Fatalln(err)
	}

	if err := json.Unmarshal(data, project); err != nil {
		log.Fatalln(err)
	}

	project.valid = true

	return nil
}

func actionSaveProject(c *cli.Context) error {
	if !project.saveOnExit {
		return nil
	}

	projectDir := c.String("config-dir")
	projectFile := path.Join(projectDir, ProjectFile)

	data, err := json.MarshalIndent(project, "", "\t")
	if err != nil {
		return err
	}

	fd, err := os.Create(projectFile)
	if err != nil {
		log.Fatalln(err)
	}
	defer fd.Close()

	if _, err := fd.Write(data); err != nil {
		log.Fatalln(err)
	}

	return nil
}

func actionProjectInit(c *cli.Context) error {
	if project.Name == "" {
		wd, err := os.Getwd()
		if err != nil {
			return err
		}
		project.Name = filepath.Base(wd)
	}

	if project.License.Type == "" || c.Bool("force") {
		project.License.Type = c.String("license-type")
	}

	if project.License.Owner == "" || c.Bool("force") {
		project.License.Owner = c.String("license-owner")
	}

	project.License.Copyright = fmt.Sprintf("%s - %s", fmt.Sprint(time.Now().Year()), fmt.Sprint(time.Now().Year()+1))

	// Generate the LICENSE file
	log.Infof("Generating %s LICENSE file.", project.License.Type)

	var licbuf bytes.Buffer
	license := generator.MustAsset(fmt.Sprintf("templates/LICENSE_%s", strings.ToUpper(project.License.Type)))

	lictmpl, err := template.New("LICENSE").Funcs(funcMap).Parse(string(license))
	if err != nil {
		return err
	}
	if err := lictmpl.Execute(&licbuf, &project.License); err != nil {
		return err
	}
	if err := ioutil.WriteFile("LICENSE", licbuf.Bytes(), 0644); err != nil {
		return err
	}

	project.saveOnExit = true

	log.Infof("Initalized hiro api project %q. Run 'hiro generate' to build the api from the spec.", project.Name)

	return nil
}

func actionEnvExport(c *cli.Context) error {
	envName := c.GlobalString("env")

	if e, ok := project.Environment[envName]; ok {
		for k, v := range e {
			fmt.Printf("%s=%q\n", k, v)
		}
	} else {
		return errors.New("invalid environment")
	}

	return nil
}

// SpecPath returns the path for the spec
func (p *Project) SpecPath(name string) string {
	if project.APIs == nil {
		return ""
	}

	specDef, ok := project.APIs[name]
	if !ok {
		return ""
	}

	return specDef.Spec
}

func actionEnvSet(c *cli.Context) error {
	if c.NArg() != 2 {
		return errors.New("invalid number of args")
	}

	name := c.Args().First()
	value := c.Args().Get(1)

	if strings.HasPrefix(value, "file://") {
		p := strings.TrimPrefix(value, "file://")
		data, err := ioutil.ReadFile(p)
		if err != nil {
			return err
		}
		value = string(data)
	}

	env := c.GlobalString("env")
	enc := c.Bool("encrypt")
	key := c.String("key")

	if enc && key == "" {
		if tmp, ok := project.Keys[env]; ok {
			key = tmp
		}
	}

	if project.Environment == nil {
		project.Environment = make(map[string]map[string]string)
	}
	e, ok := project.Environment[env]
	if !ok {
		e = make(map[string]string)
		project.Environment[env] = e
	}

	if enc {
		if key == "" {
			return errors.New("encryption key required")
		}
		val, err := aws.DefaultProvider().Encrypt([]byte(value), sparks.Params{"keyId": key})
		if err != nil {
			return err
		}
		e[name] = base64.StdEncoding.EncodeToString(val)
	} else {
		e[name] = value
	}

	project.saveOnExit = true

	return nil
}

func actionEnvGet(c *cli.Context) error {
	if c.NArg() != 1 {
		return errors.New("invalid number of args")
	}

	name := c.Args().First()

	env := c.GlobalString("env")
	dec := c.Bool("decrypt")

	e, ok := project.Environment[env]
	if !ok {
		return errors.New("unknown environment")
	}

	if dec {
		v, err := base64.StdEncoding.DecodeString(e[name])
		if err != nil {
			return err
		}
		val, err := aws.DefaultProvider().Decrypt(v, sparks.Params{})
		if err != nil {
			return err
		}
		fmt.Printf("%s = %s\r\n", name, string(val))
	} else {
		fmt.Printf("%s = %s\r\n", name, e[name])
	}

	return nil
}
