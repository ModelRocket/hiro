/*************************************************************************
 * MIT License
 * Copyright (c) 2018 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package main

import (
	"bytes"
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/rpc"
	"os"
	"os/exec"
	"os/signal"
	"path"
	"path/filepath"
	"runtime"
	"strings"
	"text/template"
	"time"

	"github.com/spf13/cast"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda/messages"
	"github.com/google/uuid"
	"github.com/gosimple/slug"
	"github.com/urfave/cli"
	"gitlab.com/ModelRocket/hiro/pkg/generator"
	"gitlab.com/ModelRocket/reno/types"
	"google.golang.org/grpc"
)

type (
	lambdaProxy struct {
		eventType  string
		returnType string
		client     interface{}
	}
)

func actionAddFunction(c *cli.Context) error {
	apiName := c.String("api")

	// just use the first api in the config
	if apiName == "" {
		for k := range project.APIs {
			apiName = k
			break
		}
	}

	spec, err := specValidate(project.SpecPath(apiName))
	if err != nil {
		log.Errorln(err)
		return errors.New("function create failed due to errors")
	}

	apiName = slug.Make(spec.Spec().Info.Title)

	opts, err := genOptsBuild(spec, "")
	if err != nil {
		return err
	}

	opts.DryRun = true
	app, err := generator.GenerateServer(apiName, []string{}, []string{}, opts)
	if err != nil {
		return err
	}

	name := packageName(apiName)

	if c.NArg() > 0 {
		name = c.Args().Get(0)
	}

	data := generator.MustAsset("templates/functions/main.gotmpl")
	functmpl, err := template.New(name).Funcs(funcMap).Parse(string(data))
	if err != nil {
		return err
	}

	log.Infof("generating function %s", name)

	outpath := c.String("output")
	if outpath == "" {
		outpath = fmt.Sprintf("functions/%s", name)
		if err := os.MkdirAll(outpath, 0700); err != nil {
			return err
		}
	}

	filename := fmt.Sprintf("%s/main.go", outpath)

	if _, err := os.Stat(filename); err != nil {
		if !os.IsNotExist(err) {
			return err
		}
	} else if !c.Bool("force") {
		log.Warnf("function %s exists, will not overwrite", name)
		return nil
	}

	var buf bytes.Buffer

	if err := functmpl.Execute(&buf, app); err != nil {
		return err
	}

	if err := writeFile(filename, buf.Bytes(), 0644); err != nil {
		return err
	}

	cmd := exec.Command("goimports", "-w", filename)
	_, err = cmd.CombinedOutput()
	if err != nil {
		return err
	}

	return nil
}

func actionExecFunction(c *cli.Context) error {
	build := c.BoolT("build")

	if c.NArg() < 1 {
		return errors.New("missing path parameter")
	}
	buildPath := c.Args().Get(0)
	exePath := buildPath

	f, ok := project.Functions[buildPath]
	if ok {
		if f.Source != "" {
			buildPath = f.Source
		} else {
			buildPath = "./" + path.Join("./functions", buildPath)
		}
	} else {
		f = &Function{}
	}
	binBase := filepath.Base(buildPath)

	if f.Proxy == "" {
		f.Proxy = c.String("proxy")
	}
	if f.Env == "" {
		f.Env = c.GlobalString("env")
	}

	// build the code at the path specified and set the new exe path
	if build {
		tmpDir, err := ioutil.TempDir("/tmp", binBase)
		if err != nil {
			return err
		}

		exePath = fmt.Sprintf("%s/%s", tmpDir, binBase)
		if runtime.GOOS == "windows" {
			exePath += ".exe"
		}

		log.Infof("build %q --> %s", binBase, exePath)

		build := exec.Command("go", "build", "-o", exePath, buildPath)
		build.Stderr = os.Stderr
		if err := build.Run(); err != nil {
			return fmt.Errorf("failed to build the function: %v", err)
		}
		defer os.RemoveAll(tmpDir)
	}

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt)

	wd, err := os.Getwd()
	if err != nil {
		return err
	}
	env := os.Environ()
	env = append(env, fmt.Sprintf("LAMBDA_TASK_ROOT=%s", wd))
	env = append(env, fmt.Sprintf("_LAMBDA_SERVER_PORT=%s", c.String("rpc-port")))

	for _, envName := range []string{"common", f.Env} {
		if e, ok := project.Environment[envName]; ok {
			for k, v := range e {
				env = append(env, fmt.Sprintf("%s=%s", k, v))
			}
		} else if envName != "common" {
			log.Warnf("env %q does not exist", envName)
		}
	}

	// read in defaults
	apexConfig := "function.json"
	if d, err := ioutil.ReadFile(path.Join("./functions", binBase, apexConfig)); err == nil {
		fc := make(types.StringMap)
		if err := json.Unmarshal(d, &fc); err == nil {
			for k, v := range fc.Sub("environment").(types.StringMap) {
				env = append(env, fmt.Sprintf("%s=%s", k, v))
			}
		}
	}

	// read in env specific
	envConfig := fmt.Sprintf("function.%s.json", f.Env)

	if d, err := ioutil.ReadFile(path.Join("./functions", binBase, envConfig)); err == nil {
		fc := make(types.StringMap)
		if err := json.Unmarshal(d, &fc); err == nil {
			for k, v := range fc.Sub("environment").(types.StringMap) {
				env = append(env, fmt.Sprintf("%s=%s", k, v))
			}
		}
	}

	args := c.Args().Tail()
	pargs := types.Slice(&args)

	// allow pass through of args and switches
	if dd := pargs.IndexOf("--"); dd >= 0 {
		pargs.Remove(dd)
	}
	cmd := exec.Command(exePath, args...)
	cmd.Env = env
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	var server *http.Server

	go func() {
		log.Printf("Starting %q with env %q: %v", binBase, f.Env, cmd.Args)

		var client interface{}

		if f.Proxy != "none" {
			go func() {
				if c.Bool("grpc") {
					for true {
						conn, err := grpc.Dial(fmt.Sprintf("127.0.0.1:%s", c.String("rpc-port")), grpc.WithInsecure())
						if err != nil {
							log.Warnf("failed to connect to lambda: %s", err.Error())
							time.Sleep(time.Second * 1)
							continue
						}
						client = conn
					}
				} else {
					for true {
						conn, err := rpc.Dial("tcp", fmt.Sprintf("127.0.0.1:%s", c.String("rpc-port")))
						if err != nil {
							log.Warnf("failed to connect to lambda: %s", err.Error())
							time.Sleep(time.Second * 1)
							continue
						}
						client = conn
						break
					}
				}

				server = &http.Server{Addr: c.String("proxy-listen"), Handler: &lambdaProxy{f.Proxy, c.String("raw-return-type"), client}}

				log.Printf("Proxy server (%s) listenting on http://%s", f.Proxy, c.String("proxy-listen"))

				if err := server.ListenAndServe(); err != nil {
					log.Fatalln(err)
				}
			}()
		}

		if err := cmd.Run(); err != nil {
			log.Errorln(err)
		}
		stop <- os.Interrupt
	}()

	<-stop

	cmd.Process.Signal(os.Interrupt)

	cmd.Wait()

	if server != nil {
		log.Println("Shutting down the server...")

		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		cancel()

		server.Shutdown(ctx)

		log.Println("Server shutdown.")
	}
	return nil
}

func (p *lambdaProxy) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var payload []byte

	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Errorln(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	switch p.eventType {
	case "apigw":
		event := events.APIGatewayProxyRequest{
			HTTPMethod: r.Method,
			Headers:    make(map[string]string),
			PathParameters: map[string]string{
				"proxy": strings.TrimPrefix(r.RequestURI, "/"),
			},
			Body:            base64.StdEncoding.EncodeToString(data),
			IsBase64Encoded: true,
		}

		for k, v := range r.Header {
			event.Headers[k] = v[0]
		}

		payload, err = json.Marshal(event)
		if err != nil {
			log.Errorln(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	case "raw":
		payload = data
	default:
		log.Fatalln("Invalid proxy mode")
	}

	var response []byte

	switch conn := p.client.(type) {
	case *grpc.ClientConn:

		w.Header().Set("content-type", p.returnType)

		client := NewFunctionClient(conn)
		resp, err := client.Invoke(context.Background(), &InvokeRequest{
			RequestId:             uuid.New().String(),
			InvokedFunctionArn:    "arn-foo",
			CognitoIdentityId:     "cognito-foo",
			CognitoIdentityPoolId: "cognito-foo",
			Payload:               payload,
		})
		if err != nil {
			log.Errorf("failed to invoke rpc: %s", err.Error())
			data, _ := json.Marshal(err)
			w.Write(data)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		response = resp.Payload

	case *rpc.Client:

		req := &messages.InvokeRequest{
			RequestId:             uuid.New().String(),
			InvokedFunctionArn:    "arn-foo",
			CognitoIdentityId:     "cognito-foo",
			CognitoIdentityPoolId: "cognito-foo",
			Payload:               payload,
		}

		resp := &messages.InvokeResponse{}

		err = conn.Call("Function.Invoke", req, resp)
		if err != nil {
			log.Errorf("failed to invoke rpc: %s", err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		response = resp.Payload
	}

	var body []byte

	switch p.eventType {
	case "apigw":
		proxyResp := &events.APIGatewayProxyResponse{}
		if err := json.Unmarshal(response, proxyResp); err != nil {
			log.Errorln(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		body = []byte(proxyResp.Body)

		for k, v := range proxyResp.Headers {
			w.Header().Add(k, v)
		}

		if proxyResp.IsBase64Encoded {
			body, err = base64.StdEncoding.DecodeString(proxyResp.Body)
			if err != nil {
				log.Errorln(err)
				w.WriteHeader(http.StatusInternalServerError)
			}
		}
		if contentLength := w.Header().Get("Content-Length"); contentLength != "" {
			body = body[:cast.ToInt(contentLength)]
		}

		w.WriteHeader(proxyResp.StatusCode)

	case "raw":
		w.Header().Set("content-type", p.returnType)
		body = response
	}

	buf := bytes.NewBuffer(body)

	io.Copy(w, buf)
}
