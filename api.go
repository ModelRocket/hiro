/*************************************************************************
 * MIT License
 * Copyright (c) 2018 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package main

import (
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"text/template"

	"github.com/blang/semver"
	"github.com/go-openapi/loads"
	"github.com/gosimple/slug"
	"gitlab.com/ModelRocket/hiro/pkg/generator"

	"github.com/go-openapi/analysis"
	"github.com/go-openapi/swag"
	"github.com/urfave/cli"
)

var (
	funcMap = generator.FuncMap

	// APISourceFiles are the common source files
	APISourceFiles = map[string]bool{
		"api.go":     false,
		"version.go": false,
	}
)

func init() {
	funcMap["packageRoot"] = func() string {
		root := ""

		wd, err := os.Getwd()
		if err != nil {
			return ""
		}
		goPath, ok := os.LookupEnv("GOPATH")
		if !ok {
			return ""
		}
		for _, p := range strings.Split(goPath, ":") {
			srcRoot := filepath.Join(p, "src")
			if strings.HasPrefix(wd, srcRoot) {
				root = strings.TrimPrefix(wd, srcRoot+"/")
				break
			}
		}

		return root
	}

	funcMap["fixalize"] = func(v string) string {
		return v
	}

	funcMap["tolower"] = strings.ToLower

	funcMap["packageName"] = packageName

	funcMap["buildVer"] = func(name, def string) string {
		a, ok := project.APIs[strings.ToLower(name)]
		if !ok {
			return def
		}

		ver, err := semver.Parse(a.Version)
		if err != nil {
			return def
		}

		hash, _ := semver.NewPRVersion(gitHash())
		ver.Pre = []semver.PRVersion{
			hash,
		}

		return ver.String()
	}

	funcMap["pascalizePackaged"] = func(name string) string {
		parts := strings.Split(name, ".")

		if len(parts) <= 1 {
			return name
		}

		parts[1] = swag.ToHumanNameTitle(parts[1])

		return strings.Join(parts, ".")
	}

	funcMap["pascalizeBase"] = func(name string) string {
		parts := strings.Split(name, ".")
		if len(parts) <= 1 {
			return name
		}
		return swag.ToHumanNameTitle(parts[1])
	}

	funcMap["pascalizeBaseTitle"] = func(name string) string {
		parts := strings.Split(name, ".")
		if len(parts) <= 1 {
			return swag.ToHumanNameTitle(name)
		}
		return swag.ToHumanNameTitle(parts[1])
	}

	funcMap["typePackage"] = func(name string) string {
		parts := strings.Split(name, ".")
		if len(parts) <= 1 {
			return ""
		}
		return strings.ToLower(parts[0]) + "."
	}

	funcMap["scalarArray"] = func(name string) string {
		if !project.ScalarArrays {
			return name
		}
		return strings.Replace(name, "[]*", "[]", 1)
	}
}

func actionAPIGenerate(c *cli.Context) error {
	if c.NArg() < 1 {
		return errors.New("missing api name argument")
	}

	return apiGenerate(c.Args().First(), "", c.Bool("force"))
}

func apiGenerate(name, target string, force bool) error {
	specFile := project.SpecPath(name)

	log.Infof("Generating api from spec at %q", specFile)

	spec, err := specValidate(specFile)
	if err != nil {
		log.Errorln(err)
		return errors.New("API generation failed due to errors")
	}

	apiName := packageName(slug.Make(spec.Spec().Info.Title))

	api := project.APIs[apiName]

	opts, err := genOptsBuild(spec, target)
	if err != nil {
		return err
	}

	basePath := "api"
	if api.BasePath != "" {
		basePath = api.BasePath
	}

	// Generate the base swagger operations and types
	app, err := generator.GenerateServer(apiName, []string{}, []string{}, opts)
	if err != nil {
		return err
	}

	log.Info("Generating api implementation files...")
	for name, overwrite := range APISourceFiles {
		var buf bytes.Buffer

		filename := fmt.Sprintf("%s/rest/%s", basePath, name)
		if _, err := os.Stat(filename); err == nil && !overwrite && !force {
			continue
		}

		bytes := generator.MustAsset(fmt.Sprintf("templates/api/%stmpl", name))
		tmpl, err := template.New(name).Funcs(funcMap).Parse(string(bytes))

		if err != nil {
			return err
		}

		if err := tmpl.Execute(&buf, app); err != nil {
			return err
		}

		if err := writeFile(filename, buf.Bytes(), 0644); err != nil {
			return err
		}
	}

	log.Info("Generating api operations stubs...")

	data := generator.MustAsset("templates/api/operation.gotmpl")
	optmpl, err := template.New("operations").Funcs(funcMap).Parse(string(data))
	if err != nil {
		return err
	}

	appendOps := false

	for _, group := range app.OperationGroups {
		var buf bytes.Buffer

		group.ParentName = app.Name
		filename := fmt.Sprintf("%s/rest/%s.go", basePath, swag.ToFileName(group.Name))
		if _, err := os.Stat(filename); err != nil {
			if !os.IsNotExist(err) {
				return err
			}
		} else if !force {
			data, err := ioutil.ReadFile(filename)
			if err != nil {
				return err
			}

			newOps := make(generator.GenOperations, 0)
			for _, o := range group.Operations {
				if index := strings.Index(string(data), pascalize(o.Name)); index == -1 {
					newOps = append(newOps, o)
				}
			}

			if len(newOps) > 0 {
				group.Operations = newOps
				group.NoOperationHeader = true
				appendOps = true
			} else {
				continue
			}
		}

		if err := optmpl.Execute(&buf, group); err != nil {
			return err
		}

		if appendOps {
			f, err := os.OpenFile(filename, os.O_APPEND|os.O_WRONLY, 0600)
			if err != nil {
				return err
			}
			defer f.Close()

			if _, err = f.WriteString("\r\n\r\n"); err != nil {
				return err
			}
			if _, err = f.Write(buf.Bytes()); err != nil {
				return err
			}
		} else if err := ioutil.WriteFile(filename, buf.Bytes(), 0644); err != nil {
			return err
		}
		// run goimports and cleanup
		cmd := exec.Command("goimports", "-w", filename)
		_, err = cmd.CombinedOutput()
		if err != nil {
			return err
		}
	}

	log.Info("API Generated successfully.")

	return nil
}

func genOptsBuild(spec *loads.Document, target string) (*generator.GenOpts, error) {
	var license []byte
	var err error

	apiName := packageName(slug.Make(spec.Spec().Info.Title))

	if project.License.Header != "" {
		license = []byte(project.License.Header)
	} else {
		license, err = ioutil.ReadFile("LICENSE")
		if err != nil {
			return nil, err
		}
	}

	api := project.APIs[apiName]

	if target == "" {
		target = "./api"
		if api.BasePath != "" {
			target = api.BasePath
		}
		if err := os.MkdirAll(target, 0700); err != nil {
			return nil, err
		}
	}

	opts := &generator.GenOpts{
		Spec:                   spec.SpecFilePath(),
		Target:                 target,
		APIPackage:             "operations",
		ModelPackage:           "models",
		ServerPackage:          "rest",
		ClientPackage:          "client",
		Principal:              api.Principal,
		DefaultScheme:          "http",
		IncludeModel:           false,
		IncludeValidator:       true,
		IncludeHandler:         true,
		IncludeParameters:      true,
		IncludeResponses:       true,
		IncludeURLBuilder:      true,
		IncludeMain:            false,
		IncludeSupport:         true,
		ValidateSpec:           true,
		ExcludeSpec:            false,
		Template:               "",
		RegenerateConfigureAPI: true,
		WithContext:            api.WithContext,
		DumpData:               false,
		Models:                 []string{},
		Operations:             []string{},
		Tags:                   []string{},
		Name:                   "",
		FlagStrategy:           "go-flags",
		CompatibilityMode:      "modern",
		ExistingModels:         api.Models,
		Copyright:              string(license),
		Imports:                map[string]string{"reno": "gitlab.com/ModelRocket/reno/types"},
	}

	opts.EnsureDefaults()
	opts.FlattenOpts = &analysis.FlattenOpts{
		//Expand:  true,
		Minimal: true,
	}

	return opts, nil
}

func actionExportTemplates(c *cli.Context) error {
	base := c.String("out")
	if err := os.MkdirAll(base, 0777); err != nil {
		return err
	}
	for name, data := range generator.ExportedAssets {
		dr := filepath.Dir(name)
		if err := os.MkdirAll(filepath.Join(base, dr), 0777); err != nil {
			return err
		}
		if err := ioutil.WriteFile(filepath.Join(base, name), data, 0700); err != nil {
			return err
		}
	}
	return nil
}

func pascalize(arg string) string {
	if len(arg) == 0 || arg[0] > '9' {
		return swag.ToGoName(arg)
	}
	if arg[0] == '+' {
		return swag.ToGoName("Plus " + arg[1:])
	}
	if arg[0] == '-' {
		return swag.ToGoName("Minus " + arg[1:])
	}

	return swag.ToGoName("Nr " + arg)
}
