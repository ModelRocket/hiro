/*************************************************************************
 * MIT License
 * Copyright (c) 2018 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package main

import (
	"bytes"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strings"

	"gitlab.com/ModelRocket/reno/util"
)

var (
	packageNameRegexp = regexp.MustCompile("[^a-zA-Z0-9]+")
)

func writeFile(filename string, data []byte, perm os.FileMode) error {
	base := filepath.Dir(filename)
	if err := os.MkdirAll(base, os.ModePerm); err != nil {
		return err
	}
	return ioutil.WriteFile(filename, data, perm)
}

func packageName(val string) string {
	return strings.ToLower(packageNameRegexp.ReplaceAllString(val, ""))
}

func gitHash() string {
	var out bytes.Buffer
	cmd := exec.Command("git", "log", "--pretty=format:'%h'", "-n", "1")
	cmd.Stdout = &out
	if err := cmd.Run(); err != nil {
		return ""
	}

	return util.Unquote(string(out.Bytes()))
}
