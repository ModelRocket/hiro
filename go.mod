module gitlab.com/ModelRocket/hiro

go 1.13

require (
	github.com/BluntSporks/abbreviation v0.0.0-20150522120346-096cdb48bafa
	github.com/aws/aws-lambda-go v1.13.3
	github.com/blang/semver v3.5.1+incompatible
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/go-openapi/analysis v0.19.7
	github.com/go-openapi/errors v0.19.3
	github.com/go-openapi/inflect v0.19.0
	github.com/go-openapi/loads v0.19.4
	github.com/go-openapi/runtime v0.19.9
	github.com/go-openapi/spec v0.19.5
	github.com/go-openapi/strfmt v0.19.4
	github.com/go-openapi/swag v0.19.6
	github.com/go-openapi/validate v0.19.5
	github.com/golang/protobuf v1.3.2
	github.com/google/shlex v0.0.0-20191202100458-e7afc7fbc510 // indirect
	github.com/google/uuid v1.1.1
	github.com/gordonklaus/ineffassign v0.0.0-20190601041439-ed7b1b5ee0f8 // indirect
	github.com/gosimple/slug v1.9.0
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/kr/pretty v0.1.0
	github.com/lunixbochs/vtclean v1.0.0 // indirect
	github.com/manifoldco/promptui v0.6.0
	github.com/mattn/go-colorable v0.1.4 // indirect
	github.com/mattn/go-isatty v0.0.11 // indirect
	github.com/pelletier/go-toml v1.6.0 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/cast v1.3.1
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/spf13/viper v1.6.1
	github.com/teris-io/shortid v0.0.0-20171029131806-771a37caa5cf
	github.com/urfave/cli v1.22.2
	gitlab.com/ModelRocket/reno v1.0.1
	gitlab.com/ModelRocket/sparks v1.6.1
	golang.org/x/lint v0.0.0-20191125180803-fdd1cda4f05f // indirect
	golang.org/x/sys v0.0.0-20191228213918-04cbcbbfeed8 // indirect
	golang.org/x/tools v0.0.0-20191227053925-7b8e75db28f4
	google.golang.org/genproto v0.0.0-20191223191004-3caeed10a8bf // indirect
	google.golang.org/grpc v1.26.0
	gopkg.in/alecthomas/kingpin.v3-unstable v3.0.0-20191105091915-95d230a53780 // indirect
	gopkg.in/ini.v1 v1.51.1 // indirect
)
